from django.urls import re_path
from .views import index
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
#url for app

urlpatterns = [
    re_path(r'^$', index, name='index'),
]
